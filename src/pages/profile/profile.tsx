import React, {useEffect, useState} from 'react';
import axios from "axios";

export interface userProfile {
    id: number;
    name: string;
    about: string;
    image: string;
}
const Profile: React.FC = () => {
    const [profile, setProfile] = useState<userProfile>();
    useEffect(() => {
        (async () => {
            const response = await axios.get('/api/profile');
            setProfile(response.data);
        })();
    }, []);

    if(!profile) return <div>...loading</div>;

    return <div>
        <section cy-data={'name'}>{profile.name}</section>
        <section cy-data={'about'}>{profile.about}</section>
        <img src={profile.image} alt={'profileImage'} cy-data={'profileImage'} />
    </div>
}

export default Profile;