import React from 'react';

const Home: React.FC = () => {
    return (
        <div className={'home'}>
            <p>This is the home page to test</p>
        </div>
    );
}

export default Home;