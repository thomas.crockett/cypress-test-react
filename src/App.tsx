import React from 'react';
import './App.css';
import NavBar from "./components/navBar/navBar";
import {Routes, Route, BrowserRouter} from "react-router-dom";
import Home from "./pages/home/home";
import Profile from "./pages/profile/profile";

function App() {

    return (
        <div className="App">
            <BrowserRouter>
                <NavBar/>
                <Routes>
                    <Route path='/' element={<Home/>}/>
                    <Route path='/profile' element={<Profile/>}/>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
