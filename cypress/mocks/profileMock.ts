// @ts-ignore
export const getProfile = (response, userInfo) => {
    let endpoint = '/api/profile';
    cy.intercept('GET', `${endpoint}?id=${userInfo.id}`, response).as('getProfile');
}