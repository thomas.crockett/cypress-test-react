import React from 'react';

describe('home page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  })
  it('renders nav bar', () => {
    cy.contains('Home');
    cy.contains('Profile');
  });

  it('renders home page', () => {
    cy.get('a').contains('Home').click();
    cy.contains('This is the home page to test');
  });

  it('renders profile page', () => {
    cy.get('a').contains('Profile').click();
    cy.contains('This is the profile page to test');
  })
})

