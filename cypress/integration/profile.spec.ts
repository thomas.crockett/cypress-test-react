import * as profileMocks from '../mocks/profileMock';
import {getProfile} from "../mocks/profileMock";

describe('Profile page', () => {
    beforeEach(() => {
       cy.server();
       cy.fixture('profile').then(rc => {
           profileMocks.getProfile(rc.getProfile, {id: 1})
       });
       cy.visit('/api/profile?id=1');
       cy.wait('@getProfile()');
    });

    it('should show the profile page', () => {
        cy.get('[data-cy=name]').should(`exist`);
        cy.get('[data-cy=name]').contains('Thomas Crockett');
    });
})